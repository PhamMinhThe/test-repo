#include "SortType.h"

void Merge(int* a, int left, int mid, int right) {
	int c1, c2, d;
	int* tmpArr = new int[right+1];

	for (c1 = left, c2 = mid + 1, d = left; c1 <= mid && c2 <= right; d++) {
		if (a[c1] > a[c2])
			tmpArr[d] = a[c2++];
		else
			tmpArr[d] = a[c1++];
	}
	for (; c1 <= mid; c1++) {
		tmpArr[d++] = a[c1];

	}
	for (; c2 <= right; c2++) {
		tmpArr[d++] = a[c2];
	}

	for (int i = left; i < right + 1; i++) {
		a[i] = tmpArr[i];
	} 

	delete[] tmpArr;
}

void MergeSort(int* a, int left, int right) 
{
	if (left < right) 
	{
		int mid = (left + right) / 2;
		MergeSort(a, left, mid);
		MergeSort(a, mid + 1, right);

		Merge(a, left, (left + right) / 2, right);
	}
}

void QuickSort(int*a, int Left, int Right)
{
	if (Left <= Right)
	{
		int Pivot = a[(Left + Right) / 2];

		int low = Left;
		int high = Right;

		while (low <= high)
		{
			while (a[low] < Pivot)
				low++;
			while (a[high] > Pivot)
				high--;

			if (low <= high)
			{
				swap(a[low], a[high]);
				low++;
				high--;
			}
		}

		if (Left < high)
			QuickSort(a, Left, high);
		if (Right > low)
			QuickSort(a, low, Right);
	}
}

void SelectionSort(int* a, int n) {
	int min;
	for (int i = 0; i < n; i++) {
		min = i;
		for (int j = i + 1; j < n; j++) {
			if (a[j] < a[min])
				min = j;
		}
		if (a[i] > a[min]) {
			int tmp = a[i];
			a[i] = a[min];
			a[min] = tmp;
		}
	}
}

void Heapify(int* a, int n, int i) {
	int child, saved;

	saved = a[i];
	while (i < n / 2) {
		child = 2 * i + 1;
		if (child < n - 1)
			if (a[child] < a[child + 1])
				child++;
		if (saved >= a[child])
			break;
		a[i] = a[child];
		i = child;
	}
	a[i] = saved;
}
void HeapSort(int* a, int n) {
	for (int i = n / 2 - 1; i >= 0; i--)
		Heapify(a, n, i);

	for (int i = n - 1; i >= 0; i--) {
		int tmp = a[i];
		a[i] = a[0];
		a[0] = tmp;

		Heapify(a, i, 0);
	}
}

int findMax(int* a, int n)
{
	int max = a[0];
	for (int i = 1; i < n; i++)
		if(a[i]>max)
			max = a[i];
	return max;
}

void countSort(int* a, int n, int counter)
{
	int* outArr;
	outArr = new int[n];
	int count[10] = { 0 };

	for (int i = 0; i < n; i++)
		count[a[i] / counter % 10]++;

	for (int i = 1; i < 10; i++)
		count[i] += count[i - 1];

	for (int i = n - 1; i >= 0; i--)
	{
		outArr[count[(a[i] / counter) % 10] - 1] = a[i];
		count[(a[i] / counter) % 10]--;
	}

	for (int i = 0; i < n; i++)
		a[i] = outArr[i];
}

void RadixSort(int* a, int n)
{
	int m = findMax(a, n);

	for (int counter = 1; m / counter > 0; counter *= 10)
		countSort(a, n, counter);

}

void printArr(int* a, int n) {
	for (int i = 0; i < n; i++) {
		cout << a[i] << ", ";
	}
	cout << endl;
}

