#include "SortType.h"

int main()
{
	int n;
	cout << " Input number of element: ";
	cin >> n;

	int* a = new int[n];

	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % n + 1;
	}


	cout << "Unsorted array: ";
	//printArr(a, n);

	char s;
	cout << "Input sort choice: q/ quick sort, m/merge sort, h/ heap sort, s/ selection sort r/radix sort: ";
	cin >> s;


	if (s == 'h')
	{
		clock_t start = clock();
		HeapSort(a, n); 
		clock_t finish = clock();

		double runtime = (double)(finish - start) / CLOCKS_PER_SEC;
		//cout << "You chose heap sort, array after sort: ";
		//printArr(a, n);
		cout << "Runtime: " << runtime << endl;
	}

	if (s == 's')
	{
		clock_t start = clock();
		
		SelectionSort(a, n);
		clock_t finish = clock();
		double runtime = (double)(finish - start) / CLOCKS_PER_SEC;

		//cout << "You chose selection sort, array after sort: ";
		//printArr(a, n);
		cout << "Runtime: " << runtime << endl;
	}

	if (s == 'q')
	{
		clock_t start = clock();
		QuickSort(a, 0, n - 1);
		clock_t finish = clock();
		double runtime = (double)(finish - start) / CLOCKS_PER_SEC;
		//cout << "You chose quick sort, array after sort: ";
		//printArr(a, n);
		cout << "Runtime: "<< runtime <<endl;
	}

	if (s == 'm')
	{
		clock_t start = clock();
		MergeSort(a, 0, n-1);
		clock_t finish = clock();
		double runtime = (double)(finish - start) / CLOCKS_PER_SEC;
		//cout << "You chose selection sort, array after sort: ";
		//printArr(a, n);
		cout << "Runtime: " << runtime << endl;
	}

	if (s == 'r')
	{
		clock_t start = clock();
		RadixSort(a, n);
		clock_t finish = clock();
		double runtime = (double)(finish - start) / CLOCKS_PER_SEC;
		//cout << "You chose selection sort, array after sort: ";
		//printArr(a, n);
		cout << "Runtime: " << runtime << endl;
	}

	delete[n] a;

	return 0;
}