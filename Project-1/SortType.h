#pragma once
#include <iostream>
#include <ctime>
using namespace std;

void Merge(int* a, int left, int mid, int right);
void MergeSort(int* a, int left, int right);

void SelectionSort(int* a, int n);

void Heapify(int* a, int n, int i);
void HeapSort(int* a, int n);

void QuickSort(int*a, int Left, int Right);

void RadixSort(int* a, int n);
int findMax(int* a, int n);
void countSort(int* a, int n, int counter);


void printArr(int* a, int n);